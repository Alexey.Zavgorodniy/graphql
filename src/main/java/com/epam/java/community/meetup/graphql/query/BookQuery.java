package com.epam.java.community.meetup.graphql.query;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.epam.java.community.meetup.graphql.domain.Book;
import com.epam.java.community.meetup.graphql.service.BookService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BookQuery implements GraphQLQueryResolver {

    @Autowired
    private BookService bookService;

    public List<Book> getBooks(int limit) {
        return bookService.findAll(limit);
    }

    public Book getBook(Long id) {
        return bookService.findById(id);
    }
}