package com.epam.java.community.meetup.graphql.repository;

import com.epam.java.community.meetup.graphql.domain.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {


}