package com.epam.java.community.meetup.graphql.service.impl;

import com.epam.java.community.meetup.graphql.domain.Author;
import com.epam.java.community.meetup.graphql.repository.AuthorRepository;
import com.epam.java.community.meetup.graphql.service.AuthorService;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthorServiceImpl implements AuthorService {

    private final AuthorRepository authorRepository;

    @Override
    public List<Author> findAll(int limit) {
        return authorRepository.findAll().stream()
                .limit(limit)
                .collect(Collectors.toList());
    }

    @Override
    public Author findById(Long id) {
        return authorRepository.findById(id)
                .orElseThrow(()-> new IllegalArgumentException("Author with such ID is absent in DB"));
    }

    @Override
    public Author create(Author author) {
        return authorRepository.save(author);
    }

    @Override
    public Author update(Author author) {
        return authorRepository.save(author);
    }

    @Override
    public void delete(Long id) {
        authorRepository.deleteById(id);
    }
}