package com.epam.java.community.meetup.graphql.service;

import java.util.List;

public interface EntityService<T> {

    T create(T t);

    T update(T t);

    void delete(Long id);

    List<T> findAll(int limit);

    T findById(Long id);
}