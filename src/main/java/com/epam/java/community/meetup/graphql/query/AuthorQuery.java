package com.epam.java.community.meetup.graphql.query;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.epam.java.community.meetup.graphql.domain.Author;
import com.epam.java.community.meetup.graphql.service.AuthorService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AuthorQuery implements GraphQLQueryResolver {

    @Autowired
    private AuthorService authorService;

    public List<Author> getAuthors(int limit) {
        return authorService.findAll(limit);
    }

    public Author getAuthor(Long id) {
        return authorService.findById(id);
    }
}