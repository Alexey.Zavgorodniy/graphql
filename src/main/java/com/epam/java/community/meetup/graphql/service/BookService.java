package com.epam.java.community.meetup.graphql.service;

import com.epam.java.community.meetup.graphql.domain.Book;

public interface BookService extends EntityService<Book> {

}