package com.epam.java.community.meetup.graphql.mutation;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.epam.java.community.meetup.graphql.domain.Author;
import com.epam.java.community.meetup.graphql.service.AuthorService;
import java.util.ArrayList;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class AuthorMutation implements GraphQLMutationResolver {

    private final AuthorService authorService;

    public Author createAuthor(String firstName, String lastName) {
        Author author = new Author();
        author.setFirstName(firstName);
        author.setLastName(lastName);
        author.setBooks(new ArrayList<>());
        return authorService.create(author);
    }

    public void removeAuthor(Long id) {
        authorService.delete(id);
    }
}