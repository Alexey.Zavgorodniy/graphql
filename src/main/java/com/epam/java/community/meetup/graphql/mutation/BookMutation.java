package com.epam.java.community.meetup.graphql.mutation;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.epam.java.community.meetup.graphql.domain.Author;
import com.epam.java.community.meetup.graphql.domain.Book;
import com.epam.java.community.meetup.graphql.service.AuthorService;
import com.epam.java.community.meetup.graphql.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class BookMutation implements GraphQLMutationResolver {

    private final BookService bookService;
    private final AuthorService authorService;

    public Book createBook(Long authorId, String title, int numberOfPages, int publishingYear) {
        Book book = new Book();
        Author authorById = authorService.findById(authorId);
        book.setAuthor(authorById);
        book.setTitle(title);
        book.setNumberOfPages(numberOfPages);
        book.setPublishingYear(publishingYear);
        Book savedBook = bookService.create(book);
        authorById.addBook(book);
        authorService.update(authorById);
        return savedBook;
    }

    public void removeBook(Long id) {
        Book bookById = bookService.findById(id);
        Author author = bookById.getAuthor();
        author.removeBook(bookById);
        authorService.update(author);
        bookService.delete(id);
    }
}