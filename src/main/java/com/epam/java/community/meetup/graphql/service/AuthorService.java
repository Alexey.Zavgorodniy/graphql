package com.epam.java.community.meetup.graphql.service;

import com.epam.java.community.meetup.graphql.domain.Author;

public interface AuthorService extends EntityService<Author> {

}